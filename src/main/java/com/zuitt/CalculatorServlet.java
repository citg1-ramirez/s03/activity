package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

public class CalculatorServlet extends HttpServlet {

	private static final long serialVersionUID = 43283931101411089L;
	
	public void init() throws ServletException{
		System.out.println("**************************************");
		System.out.println("Initiliazed connection to database.");
		System.out.println("**************************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {

		PrintWriter out = res.getWriter();
		
		out.println("<h1>You are now using the calculator application</h1>");
		out.println("<p> To use the application, input two numbers and an operation. </p>");
		out.println("<p> Hit the submit button after filling in the details. </p>");
		out.println("<p> You will get the results shown in your browser! </p>");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		int num1 = Integer.parseInt(req.getParameter("num1"));
		int num2 = Integer.parseInt(req.getParameter("num2"));
		int total = 0;
		
		String operation = req.getParameter("operation");
		String errorMessage = "";
		
		PrintWriter out = res.getWriter();
		
		if(operation.equals("add") || operation.equalsIgnoreCase("addition")) {
			total = num1 + num2;
		}
		
		else if(operation.equals("subtract") || operation.equalsIgnoreCase("subtraction")) {
			total = num1 - num2;
		}
		
		else if(operation.equals("multiply") || operation.equalsIgnoreCase("multiplication")) {
			total = num1 * num2;
		}
		
		else if(operation.equals("divide") || operation.equalsIgnoreCase("division")) {
			total = num1 / num2;
		}
		
		else{
			errorMessage = "No operation provided./Operation is wrong.";
		}
		
		
		if(errorMessage.isEmpty()) {
			out.print("<p> The two numbers you provided are: " + "<b>" + num1 + "</b>" + ", " + "<b>" + num2 + "</b>" + ".</p>");
			out.print("<p> The operation that you wanted is: " + "<b>" + operation.toLowerCase() +"</b>" + ".</p>");
			out.print("<p> The result is: " + "<b>" + total + "</b>" + "</p>");
		}
		else {
			out.print("<p>" + errorMessage + "</p>");
		}
	}
	
	
	public void destroy(){
		System.out.println("**************************************");
		System.out.println("Destroyed connection to database.");
		System.out.println("**************************************");
	}

}
